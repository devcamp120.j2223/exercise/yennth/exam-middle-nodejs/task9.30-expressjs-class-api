class User {
    constructor(id, name, position, office, age, startDate){
        this.id = id;
        this.name = name;
        this.position = position;
        this.office = office;
        this.age = age;
        this.startDate = startDate;

    }
}
let user1 = new User(1, "Airi Satou","Accountant", "Tokyo", 33, "2008/11/28" );
let user2 = new User(2, "Angelica Ramos","Chief Executive Officer (CEO)", "London", 47, "2008/11/28" );
let user3 = new User(3, "Ashton Cox","Junior Technical Author", "San Francisco", 66, "2008/11/28" );
let user4 = new User(4, "Bradley Greer","Software Engineer", "London", 41, "2008/11/28" );
let user5 = new User(5, "Brenden Wagner","Software Engineer", "San Francisco", 28, "2008/11/28" );
let user6 = new User(6, "Brielle Williamson","Integration Specialist", "New York", 61, "2008/11/28" );
let user7 = new User(7, "Bruno Nash","Software Engineer", "London", 38, "2008/11/28" );
let user8 = new User(8, "Caesar Vance","Pre-Sales Support", "New York", 21, "2008/11/28" );
let user9 = new User(9, "Cara Stevens","Sales Assistant", "New York", 46, "2008/11/28" );
let user10 = new User(10, "Cedric Kelly","Senior Javascript Developer", "Edinburgh", 22, "2008/11/28" );

const userList = [user1, user2, user3, user4, user5, user6, user7, user8, user9, user10];

module.exports = userList; 