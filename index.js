//khai báo thư viện express
const express = require('express');

const userList = require("./data.js");
const app = new express();
//sử dụng body json
app.use(express.json());
//sử dụng body unicode
app.use(express.urlencoded({
    urlencoded: true
}))
const port = 8000;

//get all users
app.get('/users', (req, res) => {
    let reqAge = req.query.age;
    if (!reqAge) {
        res.status(200).json({
            userList
        })
    }
    else if (isNaN(reqAge)) {
        res.status(400).json({
            message: "age is invalid!"
        })
    }
    else {
        let userListbyAge = userList.filter(user => user.age > reqAge)
        res.status(200).json({
            userListbyAge
        })
    }
});

//get user by id
app.get('/users/:userid', (req, res) => {
    let userId = req.params.userid;
    if (isNaN(userId)) {
        res.status(400).json({
            message: "user id is invalid!"
        })
    }
    else {
        let userById = '';
        userList.forEach(user => {
            if (user.id == userId) {
                userById = user;
            }
        });
        if (userById == "") {
            res.status(404).json({
                message: "userId not found!"
            })
        } else {
            res.status(200).json({
                userById
            })
        }
    }
});

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port} `)
})